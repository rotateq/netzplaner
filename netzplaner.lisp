(defpackage #:netzplaner
  (:use #:clim #:clim-lisp)
  (:nicknames #:np)
  (:export #:run))

(in-package #:netzplaner)

(defclass graphical-view (view)
  ())

(defparameter *graphical-view* (make-instance 'graphical-view))

(define-application-frame netzplan ()
  ((%knoten))
  (:pane :application
   :width 500 :height 500
   :display-function #'display
         :incremental-redisplay t
   :default-view *graphical-view*))

(defun display (frame pane)
  (declare (ignore frame))
  (com-present-knoten))

(defun run ()
  (run-frame-top-level (make-application-frame 'netzplan)))

(defclass knoten ()
  ((%name
    :initarg :name
    :initform (error "Der Knoten braucht einen symbolischen Namen.")
    :reader name
    :type symbol)
   (%dauer
    :initarg :dauer
    :initform (error "Der Knoten hat eine Dauer.")
    :accessor dauer
    :type (integer 0))
   #+(or)
   (%vorgänger
    :accessor vorgänger
    :type list)
   (%coordinates
    :initarg :coordinates
    :reader coordinates
    :type (cons integer))))

(defvar *knoten* (make-hash-table))

(defun make-knoten (name dauer &key x y)
  (make-instance 'knoten :name name :dauer dauer :coordinates (cons x y)))

(defmethod initialize-instance :after ((knoten knoten) &key)
  (setf (gethash (name knoten) *knoten*) knoten))

(make-knoten 'A 5 :x 5 :y 5)
(make-knoten 'B 10 :x 100 :y 100)
(make-knoten 'C 10 :x 50 :y 200)

(define-presentation-method present (knoten (type knoten) stream (view graphical-view) &key)
  (destructuring-bind (x . y) (coordinates knoten)
    (let* ((p1 (make-point x y))
           (länge 50)
           (p2 (make-point (+ x (* 2 länge))
                           (+ y länge))))
      (draw-rectangle stream p1 p2 :filled nil)
      (draw-arrow* stream (1+ (+ x (* 2 länge))) (+ y (* 1/2 länge)) (+ x (* 4 länge)) (+ y (* 1/2 länge)) :ink +red+ :line-thickness 2 :head-length 15 :head-width 10 :head-filled t)
      (draw-line* stream (+ x länge) y (+ x länge) (+ y länge))
      (draw-text* stream (string (name knoten)) (+ x (* 1/2 länge)) (+ y (* 1/2 länge)) :align-x :center :align-y :center)
      (draw-text* stream (princ-to-string (dauer knoten)) (+ x (* 3/4 länge)) (+ y (* 1/2 länge)) :align-x :center :align-y :center))))

(define-netzplan-command (com-present-knoten) ()
  (maphash #'(lambda (key value)
               (declare (ignore key))
               (present value 'knoten))
             *knoten*))

(define-netzplan-command (com-show-knoten-info :name t :menu t
                                               :keystroke (#\i :meta))
    ((knoten 'knoten :prompt "Welcher Knoten?"))
  ;; (present town 'town :view +textual-view+)
  (notify-user *application-frame*
               (format nil "~A hat eine Dauer von ~d."
                       (name knoten)
                       (dauer knoten))
               :title (format nil "Information on ~A" (name knoten))
               :text-style '(:serif :roman 15)))

(define-presentation-to-command-translator info-for-knoten
    (knoten com-show-knoten-info netzplan
          :gesture :select
          :documentation "Show info for this knoten.")
    (object)
  (list object))
